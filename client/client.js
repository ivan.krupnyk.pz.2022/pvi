const socket = io('ws://localhost:4000');

const form = document.getElementById('form');
const input = document.getElementById('messageInput');
const messages = document.getElementById('messages');
const sendButton = document.getElementById('sendButton')
document.addEventListener("DOMContentLoaded", function () {
  const loginPopup = document.getElementById("loginPopup");
  const loginForm = document.getElementById("name-form");
  const contentElements = document.querySelectorAll(".content");
  const errorMessageDisplay = document.getElementById("errorMessage");

  let currentUser = localStorage.getItem("login");



  loginForm.addEventListener("submit", addUserData);
  async function addUserData(event) {
    event.preventDefault();
    let users = [];
    await getUsers().then(data => { users = data });
    allParticipants = new Set();
    for (let i = 0; i < users.length; i++) {
      allParticipants.add(users[i].login);

    }
    updateChatParticipants();

    for (let i = 0; i < users.length; i++) {
      if (users[i].login === document.getElementById("name-input").value) {
        currentUser = document.getElementById("name-input").value;
        localStorage.setItem("login", currentUser);
        localStorage.setItem("key", users[i]._id);

        loginPopup.style.display = "none";

        document.getElementById("username").textContent = currentUser;
        showContent();
        return;
      }
    }
    const newUser = {
      login: document.getElementById("name-input").value,
    };
    socket.emit('register', newUser);
    await getUsers().then(data => { users = data });
  }


  async function getUsers() {
    const request = await fetch("http://localhost:4000/users", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });
    const fetchedData = await request.json();
    const data = fetchedData.records;

    return Object.values(data);
  }

  function showContent() {
    contentElements.forEach(element => {
      element.style.display = "block";
    });
    initChats();
  }
});


var users = [];



//socket.on('users', (receivedUsers) => {
//  users = receivedUsers;
//  console.log('Received users:', users);
//  initChats();
//});
const chats = [];
let userId;


let roomId;
let currentUser;







socket.on('chatMessage', (msg) => {
  if (msg.roomId != roomId) {
    return;
  }
  createMsgBox(msg);
});

async function getMessages() {
  const request = await fetch("http://localhost:4000/message-history", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  const fetchedData = await request.json();
  const data = fetchedData.records;

  console.log('mengadaba', data);

  data.map((message) => {
    createMsgBox(message);
  });
}
async function getUsers() {
  const request = await fetch("http://localhost:4000/users", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  });
  const fetchedData = await request.json();
  const data = fetchedData.records;

  console.log('mengadaba', data);

  users = Object.values(data);
  console.log('users', users);
  return Object.values(data);
}
function doS(event) {
  event.preventDefault();
  console.log('=')

  if (input.value) {
    socket.emit('chatMessage', {
      roomId: roomId,
      message: input.value,
      sender: userId,
    });
    console.log(input.value)
    input.value = '';
    console.log(input.value)
  }
}

sendButton.addEventListener("click", doS)

var addRoomLink = document.getElementById("addRoomLink");

document.addEventListener("DOMContentLoaded", function () {
  const addUserPopup = document.getElementById("addUserPopup");
  const addUserForm = document.getElementById("add-user-form");

  addUserForm.addEventListener("submit", async function (event) {
    event.preventDefault();
    let users = [];

    await getUsers().then(data => { users = data });



    const newUser = {
      login: document.getElementById("new-user-input").value,
    };
    socket.emit('register', newUser);
    addUserPopup.style.display = "none";
    initChats();

  });
});

async function initChats() {
  chats.push("All");
  await getUsers().then(data => { users = data })
  roomId = chats[0];

  currentUser = localStorage.getItem('login');
  for (let i = 0; i < users.length; i++) {
    if (users[i].login === currentUser) {

      userId = users[i]._id;

    }
  }

  console.log('users', users);

  for (let i = 0; i < users.length; i++) {
    if (userId !== users[i]._id) {
      chats.push(users[i]._id);
    }
  }

  initChatsHTML();
  socket.emit('joinRoom', chats[0]);
  getMessages();

  function initChatsHTML() {
    document.getElementById("chatRoom").innerHTML = ''

    document.getElementById("chatRoom").innerHTML += `<a href="#" id="addRoomLink" class="nav-link px-0 align-middle">
                                                    <span class="ms-1 d-none d-sm-inline" style="color: black;">Add room</span>
                                                              </a>

   

                                                          <div class="room" keys="All">
                                                         

                                                              <div class="chatRoomIcon"></div>
                                                              <div class="chatRoomText">All</div>
                                                          </div>`

    for (let i = 0; i < users.length; i++) {
      if (userId !== users[i]._id) {
        console.log("try insert key", users[i]._id)
        document.getElementById("chatRoom").innerHTML += `<div class="room bg-secondary border border-secondary rounded p-2 mb-2 text-center d-flex justify-content-center align-items-center" keys="${users[i]._id}">
        <div class="chatRoomIcon mr-2">
            <i class="bi bi-person-circle"></i>
        </div>
        <div class="chatRoomText">${users[i].login}</div>
    </div>
    
    
    
    `
      }
    }

    let roomsBtn = document.getElementsByClassName("room");
    let participants = new Set();
    for (let i = 0; i < roomsBtn.length; i++) {
      roomsBtn[i].addEventListener("click", (event) => {
        if (roomsBtn[i].getAttribute("keys") == "All") {
          socket.emit('joinRoom', chats[0]);
          roomId = chats[0];
          messages.innerHTML = '';
          document.getElementById("ChatRoomWith").textContent = `Chat room - All`
          allParticipants = new Set();
          for (let i = 0; i < users.length; i++) {
            allParticipants.add(users[i].login);

          }
          updateChatParticipants();

          getMessages();
        } else {
          let key1 = roomsBtn[i].getAttribute("keys");
          console.log("room - ", key1);

          let key2 = userId;

          roomId = (key1 >= key2) ? (`${key2}-${key1}`) : (`${key1}-${key2}`)
          console.log("room - ", roomId);

          let otherUser;
          for (let i = 0; i < users.length; i++) {
            if (String(key1) === users[i]._id) {
              otherUser = users[i].login;
            }
          }
          document.getElementById("ChatRoomWith").textContent = `Chat room - ${otherUser}`;
          allParticipants = new Set();
          socket.emit('joinRoom', roomId);
          allParticipants.add(otherUser);
          allParticipants.add(currentUser);

          updateChatParticipants();


          messages.innerHTML = '';
          messages.scrollTop = messages.scrollHeight;
          getMessages();
        }
      });
    }
  }
  var addRoomLink = document.getElementById("addRoomLink");
  addRoomLink.addEventListener("click", function (event) {
    event.preventDefault();
    addUserPopup.style.display = "flex";
  });
} const chatParticipants = document.createElement('div');
chatParticipants.setAttribute('id', 'chatParticipants');
chatParticipants.style.borderBottom = '1px solid #ccc';
chatParticipants.style.padding = '10px';
messages.parentElement.insertBefore(chatParticipants, messages);

let allParticipants = new Set();

function updateChatParticipants() {
  chatParticipants.innerHTML = '';

  Array.from(allParticipants).forEach(participant => {
    const participantDiv = document.createElement('div');
    participantDiv.style.display = 'inline-block';
    participantDiv.style.marginRight = '10px';
    participantDiv.style.textAlign = 'center';

    const icon = document.createElement('i');
    icon.className = 'bi bi-person-circle';
    icon.style.display = 'block';
    icon.style.fontSize = '24px';

    const name = document.createElement('span');
    name.textContent = participant;

    participantDiv.appendChild(icon);
    participantDiv.appendChild(name);
    chatParticipants.appendChild(participantDiv);
  });
}



function createMsgBox(msg) {
  let flag = (msg.sender === userId) ? (true) : (false);
  let participants = new Set();

  if (flag) {

    const item1 = document.createElement('a');
    const item2 = document.createElement('a');
    item1.innerHTML += `<ul>
    
                            <div class="message-i"> 


                                    <div class="message-i-text"><div class="message-bubble">  ${msg.message}</div>
                                    <div class="message-i-icon">${currentUser}</div>

                            </div>

                            <div "avtar"><i class="bi bi-person-circle"></i></div>

                            </ul>
                       `
    item2.innerHTML += `
                            <div class="message-o"> 
                            <div "avtar"><i class="bi bi-person-circle"></i></div>

                                    <div class="chatRoomIcon"> </div>
                                    <div class="message-o-text"> </div>
                            </div>
                       `;
    item2.setAttribute("style", "visibility: hidden; width: 1px;");
    messages.append(item1);
    messages.append(item2);



  } else {

    let otherUser;
    for (let i = 0; i < users.length; i++) {
      if (msg.sender === users[i]._id) {
        otherUser = users[i].login;
      }
    }

    const item = document.createElement('a');
    item.innerHTML += `
                              <div class="message-o"> 
                              
                              <div "avtar"><i class="bi bi-person-circle"></i></div>
                                 <div class="message-o-text"> <div class="message-bubble">${msg.message}</div>
                                 <div class="message-o-icon">${otherUser}</div>

                                 </div>
                                 
                              </div>
                         `;
    messages.append(item);
    scrollToBottom();

  }

}

function scrollToBottom() {
  var messages = document.getElementById('messages');
  messages.scrollTop = messages.scrollHeight;
}

scrollToBottom();
