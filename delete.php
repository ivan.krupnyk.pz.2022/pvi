<?php
include "config.php";
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache");

function handle_error($code, $message)
{
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['id'])) {
        handle_error(110, "The id is null");
    }

    $stmt = $conn->prepare("DELETE FROM chinazes WHERE id = :id");

    $id = $_POST['id'];
    $stmt->bindParam(':id', $id);

    if (!$stmt->execute()) {
        $errorInfo = $stmt->errorInfo();
        handle_error($errorInfo[0], $errorInfo[2]);
    }
    if ($stmt->rowCount() === 0) {
        handle_error(111, "The student wasn't found");
    }

    $response = true;
    echo json_encode($response);
    exit;
}

http_response_code(403);
echo "Requested resource is forbidden";
