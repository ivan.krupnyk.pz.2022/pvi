const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;

let _db;
let _usersCollection;
const mongoConnect = (callback) => {
  MongoClient.connect(
    "mongodb+srv://Ogirochok1939:qMqVkZD3pB5yUuah@lab6.mge6f3j.mongodb.net/"
  )
    .then((client) => {
      _db = client.db();
      callback();
      console.log("Connected!");
      _usersCollection = _db.collection("users");
    })
    .catch((err) => {
      console.log(err);
      throw err;
    });
};

const getDb = () => {
  if (_db) {
    return _db;
  }
  throw "No database found!";
};
const getUsersCollection = () => {
  if (_usersCollection) {
    return _usersCollection;
  }
  throw "No users collection found!";
};
exports.mongoConnect = mongoConnect;
exports.getDb = getDb;
exports.getUsersCollection = getUsersCollection; 
