const express = require("express");
const { createServer } = require("http");
const { Server } = require("socket.io");
const { mongoConnect } = require("./db");
const mongodb = require("mongodb");
const getDb = require("./db").getDb;
const app = express();
const cors = require("cors");

const httpServer = createServer(app);
const io = new Server(httpServer, { cors: { origin: "*" } });
let globalRoomId;

const users = [

];

app.use(cors());

async function addUsersToDatabase() {
  try {
    const db = getDb();
    const usersCollection = db.collection('_usersCollection');
    const usersFromDB = await usersCollection.find({}).toArray();

    users.push(...usersFromDB);

    console.log('Users added to database.');
  } catch (error) {
    console.error('Error adding users to database:', error);
  }
}

io.on('connection', (socket) => {
  console.log('server', 'a user connected');
  socket.on('register', async (newUser) => {
    try {
      const db = getDb();
      const usersCollection = db.collection('_usersCollection');
      await usersCollection.insertOne(newUser);

      console.log('Нового користувача зареєстровано:', newUser);

      io.emit('userRegistered', newUser);
    } catch (error) {
      console.error('Помилка реєстрації користувача:', error);
    }
  });
  socket.on('chatMessage', (data) => {
    console.log('server', data.roomId, data.sender, data.message);

    const db = getDb();
    db.collection('Cluster0').insertOne({
      roomId: data.roomId,
      message: data.message,
      sender: data.sender,
    })
    io.to(data.roomId).emit('chatMessage', {
      roomId: data.roomId,
      message: data.message,
      sender: data.sender,
    });

  });

  socket.on('joinRoom', (roomId) => {
    globalRoomId = roomId;
    socket.join(roomId);
    console.log('server', "user joined room - ", roomId);
  });

  socket.on('disconnect', () => {
    console.log('server, user disconnected');
  });
});

async function fetchMessages() {
  const db = getDb();

  const records = await db
    .collection("Cluster0")
    .find({ roomId: globalRoomId })
    .toArray();

  console.log("try to find in", globalRoomId);
  return records;
}

async function fetchUsers() {
  const db = getDb();

  const records = await db
    .collection("_usersCollection")
    .find({})
    .toArray();

  return records;
}
app.get('/users', (req, res, next) => {
  fetchUsers().then((records) => {
    console.log("records - ", records);
    res.json({ records });
  });
});
app.get('/message-history', (req, res, next) => {
  fetchMessages().then((records) => {
    console.log("records - ", records);
    res.json({ records });
  });
});

mongoConnect(() => {
  httpServer.listen(4000, () => {
    console.log('server running at http://localhost:4000');
  });
});
