<?php
$groupArr = [
    1 => "PZ-21",
    2 => "PZ-22",
    3 => "PZ-23",
    4 => "PZ-24",
    5 => "PZ-25",
    6 => "PZ-26",
    7 => "PZ-27"
];

$genderArr = [
    1 => "Male",
    2 => "Female"
];

$host = "localhost";
$dbname = "dbs";
$username = "root";
$password = "root";

try {
    $conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    $connectionError = "Connection error: " . $e->getMessage();
}
function getStudents()
{
    global $conn;
    try {
        $stmt = $conn->prepare("SELECT * FROM chinazes");
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } catch (PDOException $e) {
        $fetchError = "Error while fetching table data: " . $e->getMessage();
    }
}
