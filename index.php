<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Page</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="style.css">
    <meta name="description" content="My PWA Example">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="manifest" href="/manifest.json">
</head>

<body>
    <?php include("config.php"); ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">CSS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item d-lg-none">
                            <a class="nav-link active" aria-current="page" href="#">Students</a>
                        </li>
                        <li class="nav-item d-lg-none">
                            <a class="nav-link" href="dashboard.html">Dashboard</a>
                        </li>
                        <li class="nav-item d-lg-none">
                            <a class="nav-link" href="#">Tasks</a>
                        </li>
                    </ul>
                    <a href="/client/client.html" class="nav-link" role="button" aria-haspopup="true" aria-expanded="false">
                        <div id="notificationContainer" class="notification-container">
                            <i id="bellIcon" class="bi bi-bell pulse"></i>
                            <div id="notificationIndicator" class="notification-indicator"></div>
                        </div>
                    </a>


                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">
                            <i class="bi bi-person-circle"></i> New Message 1
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="bi bi-person-circle"></i> New Message 2
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="image.png" alt="Avatar" class="avatar">
                        Speed Wagon
                    </a>
                    <div class="dropdown-menu" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div id="sidebar" class="sidebar">
                <div class="col-2 sidebar bg-white text-white d-none d-lg-block">
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        <li class="nav-item">
                            <a href="#" class="nav-link align-middle px-0">
                                <span class="ms-1 d-none d-sm-inline" style="color: black;">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline" style="color: black;">Students</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline" style="color: black;">Tasks</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>



            <div class="modal fade" id="addStudentModal" tabindex="-1" role="dialog" aria-labelledby="addStudentModalLabel" aria-hidden="true">
                <div class="submit-message"></div>
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addStudentModalLabel">Add Student</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="addStudentForm" method="post" action="validation.php">
                                <input type="hidden" id="id">
                                <div class="form-group">
                                    <label for="studentGroup">Group</label>
                                    <select class="form-control" id="studentGroup" required>
                                        <option value="" data-group-id="0">--Select Group--</option>
                                        <?php foreach ($groupArr as $key => $group) { ?>
                                            <option value="<?= $key ?>" data-group-id="<?= $key ?>"><?= $group ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="studentName">Name</label>
                                    <input type="text" class="form-control" id="studentName" placeholder="Enter name" required>
                                </div>
                                <div class="form-group">
                                    <label for="studentSurName">SurName</label>
                                    <input type="text" class="form-control" id="studentSurName" placeholder="Enter SurName" required>
                                </div>
                                <div class="form-group">
                                    <label for="studentGender">Gender</label>
                                    <select class="form-control" id="studentGender" required>
                                        <option value="" data-gender-id="">--Select Gender--</option>
                                        <?php foreach ($genderArr as $key => $gender) { ?>
                                            <option value="<?= $key ?>" data-gender-id="<?= $key ?>"><?= $gender ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="studentDOB">Date of Birth</label>
                                    <input type="date" class="form-control" id="studentDOB" required>
                                </div>
                                <div class="form-group">
                                    <label for="studentStatus">Status</label>
                                    <select class="form-control" id="studentStatus" required>
                                        <option value="Active">Active</option>
                                        <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="error-display"></div>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button id="submitData" class="btn btn-primary" type="submit">Add</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal" id="deleteConfirmationModal" tabindex="-1">
                <div class="delete-message"></div>


                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Delete</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body text-center">
                            Are you sure about that?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger" onclick="confirmDelete()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-10">
                <h1 class="mb-3">Students</h1>
                <div class="text-right">
                    <button class="btn btn-white btn-m row-action">+</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-white">
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Group</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Birthday</th>
                                <th>Status</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $result = getStudents(); ?>

                            <?php if (isset($fetchError)) { ?>
                                <div class="server-message">
                                    <p><?php echo $fetchError; ?></p>
                                </div>
                            <?php } ?>

                            <?php foreach ($result as $row) { ?>
                                <tr id="<?= $row["id"]; ?>" data-id="<?= $row["id"]; ?>" data-group="<?= $row["group_id"]; ?>" data-first-name="<?= $row["first_name"]; ?>" data-last-name="<?= $row["last_name"]; ?>" data-gender="<?= $row["gender_id"]; ?>" data-birthday="<?= $row["birthday"]; ?>" data-status="<?= $row["status"]; ?>">

                                    <td><input type='checkbox'></td>
                                    <td class='group'><?= $groupArr[$row["group_id"]]; ?></td>
                                    <td class='name'><?= $row["first_name"] . " " . $row["last_name"]; ?></td>
                                    <td class='gender'><?= $genderArr[$row["gender_id"]]; ?></td>
                                    <td class='birthday'><?= $row["birthday"]; ?></td>
                                    <td class='status'><?= ($row["status"] == 0) ? '<div class="text-center"><i class="bi bi-circle-fill text-success"></i></div>' : '<div class="text-center"><i class="bi bi-circle-fill text-secondary"></i></div>'; ?></td>

                                    <td>
                                        <button class="delete-row" data-row-id="<?= $row['id']; ?>">
                                            <i class="bi bi-x-lg"></i>
                                        </button>
                                        <button class="row-action" data-row-action-id="<?= $row["id"]; ?>">
                                            <i class="bi bi-pencil"></i>
                                        </button>




                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
                <ul class="pagination justify-content-center">
                    <li class="page-item"><a class="page-link" href="#" style="color: rgb(2, 2, 2);">Previous</a></li>
                    <li class="page-item"><a class="page-link" href="#" style="color: rgb(2, 2, 2);">1</a></li>
                    <li class="page-item"><a class="page-link" href="#" style="color: rgb(2, 2, 2);">2</a></li>
                    <li class="page-item"><a class="page-link" href="#" style="color: rgb(2, 2, 2);">3</a></li>
                    <li class="page-item"><a class="page-link" href="#" style="color: rgb(2, 2, 2);">Next</a></li>
                </ul>

                <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
                <script src="jav.js"></script>


            </div>
        </div>
    </div>
</body>

</html>