const createStudentButton = document.getElementById('createStudentButton');
const errorDisplay = document.querySelector('.error-display');
const submitMessageDisplay = document.querySelector('.submit-message');
const errorMessageDisplay = document.querySelector('.delete-message');


const rowActionBtns = document.querySelectorAll('.row-action');
const deletBtns = document.querySelectorAll('.delete-row');
deletBtns.forEach(button => {
    delListener(button);
});

rowActionBtns.forEach(button => {
    rowListener(button);
});

function delListener(button) {
    button.addEventListener('click', function (event) {
        $('#deleteConfirmationModal').modal('show');
        let dataObj = {};
        dataObj.id = this.getAttribute('data-row-id');
        document.getElementById('deleteConfirmationModal').querySelector('.modal-footer button.btn-danger').onclick = function () {
            var row = button.closest('tr');



            sendDeleteRequest(dataObj);
            row.remove();
            $('#deleteConfirmationModal').modal('hide');

        };
    });
}


function rowListener(button) {
    button.addEventListener('click', function (event) {
        const closestRowId = this.getAttribute('data-row-action-id');
        let dataObj = {};
        dataObj.id = closestRowId;
        var closestRows = button.closest('tr');

        if (closestRowId) {
            const closestRow = document.getElementById(closestRowId);
            if (closestRow) {
                console.log(closestRowId);
                editRow(closestRows, dataObj);
            } else {
                console.error('Row with id ' + closestRowId + ' not found.');
            }
        } else {
            addStudent();
        }
    });
}

function openAuthPage() {
    window.location.href = '/client/auth.html';
}






function openAddStudentModal() {
    $('#addStudentModal').modal('show');
}
function gatherData(dataObj) {



    dataObj.group = document.querySelector('#studentGroup').value;
    dataObj.name = document.querySelector('#studentName').value;
    dataObj.surName = document.querySelector('#studentSurName').value;
    dataObj.gender = document.querySelector('#studentGender').value;
    dataObj.dob = document.querySelector('#studentDOB').value;
    dataObj.status = document.querySelector('#studentStatus').value;
    return dataObj;
}





function gatherDataF() {
    let dataObj = {};
    dataObj.id = "";
    dataObj.group = document.querySelector('#studentGroup').value;
    dataObj.name = document.querySelector('#studentName').value;
    dataObj.surName = document.querySelector('#studentSurName').value;
    dataObj.gender = document.querySelector('#studentGender').value;
    dataObj.dob = document.querySelector('#studentDOB').value;
    dataObj.status = document.querySelector('#studentStatus').value;
    return dataObj;
}



function sendSubmitRequest(data) {
    const urlEncodedData = new URLSearchParams();
    for (const key in data) {
        urlEncodedData.append(key, data[key]);
    }
    return fetch("validation.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: urlEncodedData
    })
        .then(response => response.json())
        .then(result => {

            if (!result.status) {
                if (result.error && result.error.message) {
                    submitMessageDisplay.classList.add('submit-invalid');
                    submitMessageDisplay.textContent = result.error.message;
                }
            }
            return result;
        });
}


function addStudent() {
    $('#addStudentModal').modal('show');
    document.getElementById('addStudentForm').reset();

    var createStudentButton = document.getElementById('submitData');
    createStudentButton.textContent = 'Create';

    createStudentButton.onclick = async function () {

        var groupId = document.getElementById('studentGroup').value;
        var group = convertGroupIdToGroup(groupId);

        var name = document.getElementById('studentName').value;

        var gender = convertGenderIdToGender(document.getElementById('studentGender').value);
        var dob = document.getElementById('studentDOB').value;
        var status = document.getElementById('studentStatus').value;

        var table = document.querySelector('table tbody');
        var newRow = table.insertRow(table.rows.length);

        const result = await sendSubmitRequest(gatherDataF());
        if (result.status == false) {
            if (result.error && result.error.message) {
                errorDisplay.style.display = 'block';
                errorDisplay.textContent = result.error.message;
            }
            return;
        }


        newRow.insertCell(0).innerHTML = '<input type="checkbox">';
        newRow.insertCell(1).textContent = group;
        newRow.insertCell(2).textContent = name;
        newRow.insertCell(3).textContent = gender;
        newRow.insertCell(4).textContent = dob;
        newRow.insertCell(5).innerHTML = status === 'Active' ? '<div class="text-center"><i class="bi bi-circle-fill text-success"></i></div>' : '<div class="text-center"><i class="bi bi-circle-fill text-secondary"></i></div>';
        newRow.insertCell(6).innerHTML = '<button class="delete-row" > <i class="bi bi-x-lg"> </i> </button><button class="row-action" data-row-action-id><i class="bi bi-pencil"></i></button>';

        delListener(newRow.querySelector('.delete-row'));
        rowListener(newRow.querySelector('.row-action'));


        $('#addStudentModal').modal('hide');





    };
}

function sendDeleteRequest(data) {
    const urlEncodedData = new URLSearchParams();
    for (const key in data) {
        urlEncodedData.append(key, data[key]);
    }

    return fetch("delete.php", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        body: urlEncodedData
    })
        .then(response => response.json())
        .then(result => {

            if (!result.status) {
                if (result.error && result.error.message) {
                    errorMessageDisplay.classList.add('delete-invalid');
                    errorMessageDisplay.textContent = result.error.message;
                }
            }
            return result.status;
        });
}




function showNotification() {
    var bellIcon = document.getElementById('bellIcon');
    var notificationIndicator = document.getElementById('notificationIndicator');

    notificationIndicator.classList.add('blink');

    setTimeout(function () {
        notificationIndicator.classList.remove('blink');
    }, 5000);
}
function editRow(rowIndex, dataObj) {
    document.getElementById('addStudentForm').reset();

    var row = rowIndex;

    var group = row.cells[1].textContent.trim();
    var firstName = row.cells[2].textContent.trim();
    var gender = row.cells[3].textContent.trim();
    var dob = row.cells[4].textContent.trim();
    var statusIcon = row.cells[5].querySelector('i');
    var statusValue = statusIcon.classList.contains('text-success') ? 'Active' : 'Inactive';
    addStudentForm.querySelector('#studentDOB').value = dob;
    setSelectedOptionByText('studentGroup', group);
    setSelectedOptionByText('studentGender', gender);
    var nameParts = firstName.split(' ');

    firstName = nameParts[0];
    lastName = nameParts.slice(1).join(' ');
    document.getElementById('studentName').value = firstName;
    document.getElementById('studentSurName').value = lastName;
    document.getElementById('studentStatus').value = statusValue;
    var createStudentButton = document.getElementById('submitData');
    createStudentButton.textContent = 'Save';

    createStudentButton.onclick = function () {
        saveEditedRow(row, dataObj);
    };

    $('#addStudentModal').modal('show');
}



async function saveEditedRow(row, dataObj) {

    var groupId = document.getElementById('studentGroup').value;


    var editedGroup = convertGroupIdToGroup(groupId);
    var editedName = document.getElementById('studentName').value;
    var editedSurName = document.getElementById('studentSurName').value;
    var editedGender = convertGenderIdToGender(document.getElementById('studentGender').value);
    var editedDOB = document.getElementById('studentDOB').value;
    var editedStatus = document.getElementById('studentStatus').value;
    row.cells[1].textContent = editedGroup;
    row.cells[2].textContent = editedName + " " + editedSurName;
    row.cells[3].textContent = editedGender;
    row.cells[4].textContent = editedDOB;
    row.cells[5].innerHTML = editedStatus === 'Active' ? '<div class="text-center"><i class="bi bi-circle-fill text-success"></i></div>' : '<div class="text-center"><i class="bi bi-circle-fill text-secondary"></i></div>';


    const result = await sendSubmitRequest(gatherData(dataObj));

    if (result.status == false) {
        if (result.error && result.error.message) {
            errorDisplay.style.display = 'block';
            errorDisplay.textContent = result.error.message;
        }
        return;
    }



    $('#addStudentModal').modal('hide');

}
function setSelectedOptionByText(selectId, text) {
    var select = document.getElementById(selectId);
    for (var i = 0; i < select.options.length; i++) {
        if (select.options[i].text === text) {
            select.selectedIndex = i;
            break;
        }
    }
}

function gatherFormData() {
    var formData = {
        group: $('#studentGroup').val(),
        name: $('#studentName').val(),
        gender: $('#studentGender').val(),
        dob: $('#studentDOB').val(),
        status: $('#studentStatus').val()
    };
    return JSON.stringify(formData);
}



function convertGroupIdToGroup(groupId) {
    const groupMapping = {
        1: 'PZ-21',
        2: 'PZ-22',
        3: 'PZ-23',
        4: 'PZ-24',
        5: 'PZ-25',
        6: 'PZ-26',
        7: 'PZ-27',
    };

    if (groupId in groupMapping) {
        return groupMapping[groupId];
    } else {
        return 'Unknown';
    }
}
function convertGenderIdToGender(genderId) {
    const genderMapping = {
        1: 'Male',
        2: 'Female',
    };

    if (genderId in genderMapping) {
        return genderMapping[genderId];
    } else {
        return 'Unknown';
    }
}
