<?php
header("Content-Type: application/json");
header("Access-Control-Allow-Origin: *");
header("Cache-Control: no-cache");
include "config.php";


function handle_error($code, $message)
{
    $response["status"] = false;
    $response["error"]["code"] = $code;
    $response["error"]["message"] = $message;
    echo json_encode($response);
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {



    if (empty($_POST['group'])) {
        handle_error(101, "Choose the group from the dropdown list");
    } else if (!array_key_exists($_POST['group'], $groupArr)) {
        handle_error(102, "Invalid group");
    }
    if (empty($_POST['name'])) {
        handle_error(103, "First name field can not be empty");
    }
    if (empty($_POST['surName'])) {
        handle_error(104, "Last name field can not be empty");
    }
    if (empty($_POST['gender'])) {
        handle_error(105, "Choose the gender from the dropdown list");
    } else if (!array_key_exists($_POST['gender'], $genderArr)) {
        handle_error(106, "Invalid gender");
    }
    if (empty($_POST['dob'])) {
        handle_error(107, "Choose the date of birth from the dropdown calendar");
    }


    $id = $_POST['id'];

    $group = $_POST['group'];
    $first_name = $_POST['name'];
    $last_name = $_POST['surName'];
    $gender = $_POST['gender'];
    $birthday = date('Y-m-d', strtotime($_POST['dob']));
    $status = $_POST['status'];



    if ($id) {
        $stmt = $conn->prepare("UPDATE chinazes SET `group_id` = :group, `first_name` = :first_name, `last_name`= :last_name,
        `gender_id` = :gender, `birthday` = :birthday, `status` = :status WHERE `id` = :id");
    } else {
        $stmt = $conn->prepare("INSERT INTO chinazes (`group_id`, `first_name`, `last_name`, `gender_id`, `birthday`, `status`) 
                                        VALUES (:group, :first_name, :last_name, :gender, :birthday, :status)");
    }

    $stmt->bindParam(':group', $group);
    $stmt->bindParam(':first_name', $first_name);
    $stmt->bindParam(':last_name', $last_name);
    $stmt->bindParam(':gender', $gender);
    $stmt->bindParam(':birthday', $birthday);
    $stmt->bindParam(':status', $status, PDO::PARAM_INT);

    if ($id === "") {
        $stmt->execute();
        $response["id"] = $conn->lastInsertId();
    } else {
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $response["id"] = $id;
    }




    $response = true;
    echo json_encode($response);
    exit;
}
